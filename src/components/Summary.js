import React from "react";

export default function Summary({ questions }) {
    return (
        <div className="summary">
            <h2>Thank you for participating, your answers were:</h2>
            <ul>
                {questions.map((question, index) => (
                    <li key={index}>
                        {`${question.question} Your answer: ` +
                            (question.type === "SELECT_MORE"
                                ? question.answer.join(",")
                                : question.answer)}
                    </li>
                ))}
            </ul>
        </div>
    );
}
