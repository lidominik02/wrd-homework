import React, { useState } from "react";
import SelectOneQuestion from "./SelectOneQuestion";
import SelectMoreQuestion from "./SelectMoreQuestion";
import FreeTextQuestion from "./FreeTextQuestion";
import { SelectEnum } from "./PollApp";

// question: "Which javascript framework do you like the most?",
//         type: "SELECT_ONE",
//         answers: ["Angular", "React", "Vue", "Svelte"]

export default function QuestionCard({
    question,
    onSelectQuestion,
    onAnswerQuestion,
    onFinalize,
}) {
    const [nextButtonText, setNextButtonText] = useState("Next");
    const handleSelectQuestionClick = (e) => {
        if (nextButtonText === "Finalize") {
            onFinalize();
        }

        let selectState = "";

        if (e.target.innerText === "Prev") selectState = onSelectQuestion(-1);
        if (e.target.innerText === "Next") selectState = onSelectQuestion(1);

        if (selectState === SelectEnum.QUESTION_NOT_ANSWERED) {
            e.target.classList.add("validationMessage");
            setTimeout(() => {
                e.target.classList.remove("validationMessage");
            }, 1000);
        }

        setNextButtonText(
            selectState === SelectEnum.END_OF_QUESTIONS ? "Finalize" : "Next"
        );
    };

    const getQuestionComponent = (type) => {
        if (type === "SELECT_ONE")
            return (
                <SelectOneQuestion
                    question={question}
                    onAnswerQuestion={onAnswerQuestion}></SelectOneQuestion>
            );

        if (type === "SELECT_MORE")
            return (
                <SelectMoreQuestion
                    question={question}
                    onAnswerQuestion={onAnswerQuestion}></SelectMoreQuestion>
            );

        if (type === "FREE_TEXT")
            return (
                <FreeTextQuestion
                    question={question}
                    onAnswerQuestion={onAnswerQuestion}></FreeTextQuestion>
            );
    };

    return (
        <div className="questionCard">
            <button className="btn" onClick={handleSelectQuestionClick}>
                Prev
            </button>
            {getQuestionComponent(question.type)}
            <button className="btn" onClick={handleSelectQuestionClick}>
                {nextButtonText}
            </button>
        </div>
    );
}
