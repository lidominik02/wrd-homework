import React from "react";

export default function SelectMoreQuestion({ question, onAnswerQuestion }) {
    const handleOnChange = (e) => {
        console.log(e.target.checked, "check");
        if (e.target.checked) {
            onAnswerQuestion([...question.answer, e.target.value]);
        } else {
            onAnswerQuestion(
                question.answer.filter((a) => a !== e.target.value)
            );
        }
    };
    return (
        <div className="question">
            <h1>{question.question}</h1>
            {question.answers.map((answerOption, index) => (
                <React.Fragment key={index}>
                    {/* <input type="checkbox" key={index} /> {answerOption} <br /> */}
                    <input
                        type="checkbox"
                        key={index}
                        value={answerOption}
                        checked={question.answer.includes(answerOption)}
                        onChange={handleOnChange}
                    />
                    {answerOption} <br />
                </React.Fragment>
            ))}
        </div>
    );
}
