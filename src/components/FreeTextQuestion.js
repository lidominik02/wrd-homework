import React from "react";

export default function FreeTextQuestion({ question, onAnswerQuestion }) {
    const handleOnChange = (e) => {
        onAnswerQuestion(e.target.value);
    };
    return (
        <div className="question">
            <h1>{question.question}</h1>
            <textarea
                name=""
                id=""
                cols="30"
                rows="10"
                onChange={handleOnChange}></textarea>
        </div>
    );
}
