import React, { useMemo, useState } from "react";
import QuestionCard from "./QuestionCard";
import Summary from "./Summary";
// import { BrowserRouter, Routes, Route, Link } from "react-router-dom";

export const SelectEnum = {
    END_OF_QUESTIONS: 0,
    START_OF_QUESTIONS: 1,
    QUESTIONS_AVAILABLE: 2,
    QUESTIONS_NOT_AVAILABLE: 3,
    QUESTION_NOT_ANSWERED: 4,
};

const ScreenEnum = {
    QUESTIONS: 0,
    SUMMARY: 1,
};

export default function PollApp({ input }) {
    const [questions, setQuestions] = useState(
        input.map((item) => {
            return {
                ...item,
                answer: item.type === "SELECT_MORE" ? [] : null,
            };
        })
    );
    const [selectedQuestionIndex, setSelectedQuestionIndex] = useState(0);
    const [screen, setScreen] = useState(ScreenEnum.QUESTIONS);

    const getSelectedQuestion = useMemo(
        () => questions[selectedQuestionIndex],
        [questions, selectedQuestionIndex]
    );

    const handleSelectQuestion = (diff) => {
        const newIndex = selectedQuestionIndex + diff;
        if (newIndex >= 0 && newIndex < questions.length) {
            if (
                selectedQuestionIndex < newIndex &&
                (questions[selectedQuestionIndex].answer === null ||
                    questions[selectedQuestionIndex].answer.length === 0)
            ) {
                return SelectEnum.QUESTION_NOT_ANSWERED;
            }

            setSelectedQuestionIndex(newIndex);

            if (newIndex === 0) return SelectEnum.START_OF_QUESTIONS;
            if (newIndex === questions.length - 1)
                return SelectEnum.END_OF_QUESTIONS;

            return SelectEnum.QUESTIONS_AVAILABLE;
        }
        return SelectEnum.QUESTIONS_NOT_AVAILABLE;
    };

    const handleAnswerQuestion = (answer) => {
        console.log(answer);

        setQuestions(
            questions.map((q, index) => {
                return index === selectedQuestionIndex
                    ? {
                          ...q,
                          answer,
                      }
                    : q;
            })
        );

        console.log(questions);
    };

    const handleFinalize = () => {
        setScreen(ScreenEnum.SUMMARY);
    };

    // console.log(questions);
    return (
        // <BrowserRouter>
        //     <Routes>
        <div className="container">
            {screen === ScreenEnum.QUESTIONS ? (
                <QuestionCard
                    question={getSelectedQuestion}
                    onSelectQuestion={handleSelectQuestion}
                    onAnswerQuestion={handleAnswerQuestion}
                    onFinalize={handleFinalize}></QuestionCard>
            ) : (
                <Summary questions={questions}></Summary>
            )}
        </div>
        //     </Routes>
        // </BrowserRouter>
    );
}
