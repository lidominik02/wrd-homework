import React from "react";

export default function SelectOneQuestion({ question, onAnswerQuestion }) {
    const handleOnChange = (e) => {
        if (e.target.checked) {
            onAnswerQuestion(e.target.value);
        }
    };

    return (
        <div className="question">
            <h1>{question.question}</h1>
            {question.answers.map((answerOption, index) => (
                <React.Fragment key={index}>
                    <input
                        type="radio"
                        value={answerOption}
                        checked={question.answer === answerOption}
                        onChange={handleOnChange}
                    />{" "}
                    {answerOption} <br />
                </React.Fragment>
            ))}
        </div>
    );
}
